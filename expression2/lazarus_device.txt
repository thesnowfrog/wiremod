@name lazarus device
@inputs On Main:entity
@outputs 
@persist Cloaked
@trigger 
interval(100)
Cloak = "models/shadertest/predator"
Cloak2 = "models/props_c17/fisheyelens"
Uncloak = "WTP/metal_5"

Results=findInSphere(Main:pos(),200)
F = findToArray()

if (On & !Cloaked) {
    entity():setMaterial(Cloak)
    for (I=1,Results) {
        Entity = F[I,entity]
        Entity:setMaterial(Cloak)
    }
    Cloaked = 1
}
if (!On & Cloaked) {
    entity():setMaterial(Uncloak)
    for (I=1,Results) {
        Entity = F[I,entity]
        Entity:setMaterial(Uncloak)
    }
    Cloaked = 0
}    
