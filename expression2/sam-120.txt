@name SAM-120
@inputs Online Fire Auto Radar:wirelink FCR:wirelink
@inputs L1:wirelink L2:wirelink L3:wirelink L4:wirelink L5:wirelink
@outputs Bogeys Alarm FireAlarm Ready
@persist Firing SalvoTimer
@trigger 
interval(100)

Hit = Radar["1",number]
T = Radar["1_Ent",entity]

if (Online==1 & Hit!=0) {Alarm=1}
else {Alarm=0}

Targets = FCR["F",array]
Bogeys = FCR["Bogeys",number]
Launchers = array()
Launchers[1,wirelink] = L1
Launchers[2,wirelink] = L2
Launchers[3,wirelink] = L3
Launchers[4,wirelink] = L4
Launchers[5,wirelink] = L5

Ready = (Launchers[1,wirelink]["Ready",number]==1 &
    Launchers[2,wirelink]["Ready",number]==1 &
    Launchers[3,wirelink]["Ready",number]==1 &
    Launchers[4,wirelink]["Ready",number]==1 &
    Launchers[5,wirelink]["Ready",number]==1)

if (Alarm) {
    for(I=1, Bogeys) {
        Launchers[I,wirelink]["Target Ent",entity] = Targets[I,entity]
    }
}

if (Alarm==1 & Fire==1) {FireAlarm=1}
else {FireAlarm=0}

if (Alarm & Fire) {
    if (SalvoTimer>=0 & Bogeys>0) {Launchers[1,wirelink]["Fire",number] = 1}
    if (SalvoTimer>=0.25 & Bogeys>1) {Launchers[2,wirelink]["Fire",number] = 1}
    if (SalvoTimer>=0.5 & Bogeys>2) {Launchers[3,wirelink]["Fire",number] = 1}
    if (SalvoTimer>=0.75 & Bogeys>3) {Launchers[4,wirelink]["Fire",number] = 1}
    if (SalvoTimer>=1 & Bogeys>4) {Launchers[5,wirelink]["Fire",number] = 1}
    SalvoTimer += 0.1
}
else {
    SalvoTimer = 0
    Launchers[1,wirelink]["Fire",number] = 0
    Launchers[2,wirelink]["Fire",number] = 0
    Launchers[3,wirelink]["Fire",number] = 0
    Launchers[4,wirelink]["Fire",number] = 0
    Launchers[5,wirelink]["Fire",number] = 0
}
