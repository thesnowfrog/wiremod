@name ROM Cartridge
@inputs 
@outputs ProgName:string MemArray:array 
@persist MemArray:array 
@trigger 
ProgName = "Test Program"

if (first() | duped()) {
    MemArray = array(
    "print Test program",
    "setmem 1 0",
    "setreg 2 1",
    "mem2reg 1 1",
    "add 1 2",
    "op2mem 1",
    "op2reg 1",
    "printv Value is",
    "setreg 2 10",
    "ifless 1 2",
    "condjmp 3",
    "stop"
    )
}
