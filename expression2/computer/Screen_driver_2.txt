@name Screen driver 2
@inputs CS:wirelink ScreenBuffer:array KB:string Input InputString:string Clk
@outputs ClrScrn Ack #ScreenBuffer:array
@persist LastScreen:array
@trigger Clk Input

#interval(100)
if (duped() | first()) {ScreenBuffer=array("","","","","","","","","","","","","","","","","","")}

ScreenBuffer[1,string] = "> "+KB

if (Input & ~Input & InputString!="") {Ack=1}
else {Ack=0}

if (ScreenBuffer:concat()!=LastScreen:concat()) {ClrScrn=1}
else {ClrScrn=0}
for (I=1,18) {
    CS:writeString(ScreenBuffer[I,string],0,I-1,90)
} 
LastScreen=ScreenBuffer
