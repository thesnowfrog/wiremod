@name Screen driver
@inputs CS:wirelink ScreenBuffer:array KB:string Input InputString
@outputs ClrScrn ScreenBuffer:array
@persist LastScreen:array
@trigger 

interval(100)
if (duped() | first()) {ScreenBuffer=array("","","","","","","","","","","","","","","","","","")}

ScreenBuffer[1,string] = "> "+KB

if (ScreenBuffer:concat()!=LastScreen:concat()) {ClrScrn=1}
else {ClrScrn=0}
for (I=1,18) {
    CS:writeString(ScreenBuffer[I,string],0,I-1,90)
} 
LastScreen=ScreenBuffer
