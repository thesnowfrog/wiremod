@name Berkut-2 FC Console
@inputs CS:wirelink Salvo MTT Launcher:wirelink FCU:wirelink
@outputs 
@persist 
@trigger 
interval(500)
for (I=1,18) {
    CS:writeString("                                                                                 ",0,I-1,90)
} 

Green = 90
Red = 900

CS:writeString("BERKUT-2 FIRE CONTROL SYSTEM",0,0,90)
CS:writeString("Missiles ready:"+toString(Launcher["Missiles",number]),0,1,90)
CS:writeString("Bogeys:"+toString(FCU["Bogeys",number]),0,2,90)

if (Launcher["Ready1",number] & Launcher["RTF1",number]) {C1 = 90}
else {C1 = 900}
if (Launcher["Ready2",number] & Launcher["RTF2",number]) {C2 = 90}
else {C2 = 900}
if (Launcher["Ready3",number] & Launcher["RTF3",number]) {C3 = 90}
else {C3 = 900}
if (Launcher["Ready4",number] & Launcher["RTF4",number]) {C4 = 90}
else {C4 = 900}

CS:writeString("M1T:"+toString(FCU["MissileTargets",table][1,entity]),0,3,C1)
CS:writeString("M2T:"+toString(FCU["MissileTargets",table][2,entity]),0,4,C2)
CS:writeString("M3T:"+toString(FCU["MissileTargets",table][3,entity]),0,5,C3)
CS:writeString("M4T:"+toString(FCU["MissileTargets",table][4,entity]),0,6,C4)

if (FCU["Launching",number]) {CS:writeString("LAUNCHING",0,15,900)}

if (Salvo) {CS:writeString("SALVO MODE ON",0,16,90)}
else {CS:writeString("SALVO MODE OFF",0,16,900)}
if (MTT) {CS:writeString("MULTI-TARGET MODE ON",0,17,90)}
else {CS:writeString("MULTI-TARGET MODE OFF",0,17,900)}
