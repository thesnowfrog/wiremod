@name HK-106 3.0 Hunter-Killer Processor
@inputs On Bogeys AimTurret Fuel LaserDest:vector AutoDest:vector Mode TurretEnt:entity Gun:entity C2Dest:vector C2Override
@outputs Active Engage Move Moving EntID GPS:vector Destination:vector PatrolMode Dist Safeaimvec:vector EngageTimer MoveTimer
@persist GPS:vector Destination:vector Engage Move EngageTimer MoveTimer
@trigger On 

I=100
interval(100)
Null=vec(0,0,0)
Active = On
if (first() | duped()) {Move=1}

if (On) {
    
    # OPERATING MODE
    if (C2Override) { # COMMAND AND CONTROL OVERRIDE
        Destination = C2Dest
    }
    elseif (Mode == 0) { # MANUAL MODE
        Destination = LaserDest
    }
    elseif (Mode == 1) { #PATROL MODE
        PatrolMode = 1
        Destination = AutoDest
    }
        
    # NAVIGATION
    E=entity()
    EntID = E:id()
    GPS= E:pos()
    Gx=GPS:x()
    Py=GPS:y()
    Sz=GPS:z()
    Hdg=E:angles():yaw()
    Bear=E:bearing(Destination)
    Dist=E:pos():distance(Destination)
    SafeaimvecX=Gx+(cos(Hdg)*1000)
    SafeaimvecY=Py+(sin(Hdg)*1000)
    SafeaimvecZ=Sz+100
    Safeaimvec=vec(SafeaimvecX,SafeaimvecY,SafeaimvecZ)
    Moving = Dist>200
    
    if (Move) {MoveTimer+=I}
    if (Engage) {EngageTimer+=I}
    
    MoveMax = 5000
    EngageMax = 5000
    if (!Move & !Engage) {Move=1}
    if (Moving) {
        if (!Bogeys) {
            Move = 1
            Engage = 0
            EngageTimer = 0
            }
        else {
            
            if (MoveTimer>MoveMax) {
                Engage = 1
                }
            if (Engage & EngageTimer>500 & AimTurret) {
                Move = 0
                MoveTimer = 0
            }
            elseif ((Engage & EngageTimer>500 & !AimTurret) | (!Move & EngageTimer>EngageMax)) {
                Move = 1
                EngageTimer = 0
                Engage = 0
            }
        }
    }
    else {Engage = 1
        Move = 0
        MoveTimer=0}
    

        
}
else {Move = 0
    Engage = 0
    Moving = 0
}

