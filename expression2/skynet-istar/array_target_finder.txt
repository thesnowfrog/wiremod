@name E2 Target Finder
@inputs On Index Reset
@inputs NPCs Players Props Locators Vehicles ExcludePlayer ExcludePlayerProps
@outputs Contacts:array Bogeys Entity:entity ID Distance Type:string Model:string Position:vector Velocity:vector 
@outputs IsPlayer IsNPC IsVehicle
@persist 
@trigger Index

#Exclude/Allow = blacklist
#Include/Disallow = whitelist
#blacklist priority

interval(100)
    
Ind=Index+1

if (first()|duped()) {
    findExcludeEntity(entity())
    findExcludeClass("beam")
    findExcludeClass("point_spotlight")
    findExcludeClass("spotlight_end")
    findExcludeClass("func_button")
    findExcludeClass("func_door")
    findExcludeClass("path_track")
    findExcludeClass("lua_run")
    findExcludeClass("path_corner")
    findExcludeClass("func_brush")
    findExcludeClass("npc_heli_avoidbox")
    findExcludeClass("ambient_generic")
    findExcludeClass("trigger_multiple") 
    findExcludeClass("vehicle") ######
    findExcludeClass("gmod_camera")
    findExcludeClass("hologram")
    findExcludeClass("gmod_tool")
    findExcludeClass("weapon")
    findExcludeClass("pist_weapon")
    findExcludeClass("ammo")
    findExcludeModel("weapon")
    findExcludeClass("gb5")
    findExcludeClass("env")

}
if (On) {
    E=entity()
    Owner=owner()
    Mypos=E:pos()
    Range = 30000
    MyAng=E:angles()
    
    if (ExcludePlayer) {findExcludePlayer(Owner)}
    else {findIncludePlayer(Owner)}
    
    if (ExcludePlayerProps) {findExcludePlayerProps(Owner)}
    else {findIncludePlayerProps(Owner)}
    
    if (Players) {findAllowClass("player")}
    else {findExcludeClass("player")}
    
    if (NPCs) {findAllowClass("npc")}
    else {findExcludeClass("npc")}
    
    if (Props) {
        findAllowClass("prop")
        findAllowClass("acf")
        findAllowClass("wiremod")
        findAllowClass("gmod_wire")
        findAllowClass("gmod_ent_ttc")
    }
    else {
        findExcludeClass("prop")
        findExcludeClass("acf")
        findExcludeClass("wiremod")
        findExcludeClass("gmod_wire")
        findExcludeClass("gmod_ent_ttc")
    }
    
    if (Locators) {findAllowClass("gmod_wire_locator")}
    else {findExcludeClass("gmod_wire_locator")}
    
    findInSphere(Mypos, Range)
    findSortByDistance(Mypos)
    Contacts = findToArray()
    Bogeys = Contacts:count()
    Entity = Contacts[Ind,entity]
    IsPlayer = Entity:isPlayer()
    IsNPC = Entity:isNPC()
    IsVehicle = Entity:isVehicle()
    ID = Entity:id()
    Type = Entity:type()
    Model = Entity:model()
    Position = Entity:pos()
    Velocity = Entity:vel()
    Distance = Mypos:distance(Position)
}
else{
    Entity = entity(0)
    IsPlayer = 0
    IsNPC = 0
    IsVehicle = 0
    ID = 0
    Type = ""
    Model = ""
    Position = vec()
    Velocity = vec()
    Distance = inf()
}
