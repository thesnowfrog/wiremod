@name Supply Autopilot
@inputs Go Return AddrWrite Vec:vector Clk Mode
@outputs Run AddrRead AutoVec:vector Memory:vector Dist Hit ReadDir WL
@persist Run Waypoints:table AddrRead ReadDir
@trigger Clk Go Return
interval(250)
TimeWait=30
GPS=entity():pos()

if (Clk & ~Clk) {Waypoints[toString(AddrWrite),vector] = Vec}
Destination = Waypoints[toString(AddrRead),vector]
Memory = Waypoints[toString(AddrWrite),vector]
WL = Waypoints:count()-1
Dist = Destination:distance(GPS)

if (AddrRead==0 & Dist<200) {
    Run = 0
    if (Go) {
        Run=1
        ReadDir=0
        Hit=1
        AddrRead = AddrRead+1
       }
    }
elseif (Dist<200) {
    Hit=1
    
    if (AddrRead<WL & ReadDir==0) {AddrRead = AddrRead+1}
    
    elseif (AddrRead==WL) {
        Run = 0
        if (Return) {
            ReadDir=1
            AddrRead = AddrRead-1
            Run=1
           }
        }
    elseif (ReadDir==1) {AddrRead = AddrRead-1}
    
    }
else {Hit=0}
    
if (Run) { AutoVec = Destination}
else {AutoVec = GPS}
