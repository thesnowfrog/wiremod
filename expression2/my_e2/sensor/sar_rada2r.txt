@name Targeting Radar
@inputs On Res Rate Zoom World LCD:wirelink Water
@outputs Inc XO YO D X Y G Mem:table SeenTime:table Contacts LastSeen
@persist X Y MinDepth MaxDepth FindMin FindMax LID TC Mem:table SeenTime:table

#Rate is the number of times per second for full update
#For zoom, 1 = 90 degress, larger values equal small degress

#R = TD/10000 
#Z = R*50

if (On==1) {
    Mult = 16 #16 this is the number of copies of the scan code there is
    Range=15000
    if (Res == 0) {Res = 32}
    if (Rate == 0) {Rate = 5}
    if (Zoom == 0) {Zoom = 5}
    #XO & YO are offsets
    Inc = 2 / Res #increment between each offset
    interval(1000 / Rate * Mult / Res ^ 2 )
    FindMax=Range
    FindMin=100
    MaxTime = 5
    Contacts = Mem:count()
    for (I=1,Contacts) {
        Contact = Mem:keys()[I,string]
        LastSeen = curtime() - SeenTime[Contact,number]
        if (LastSeen > MaxTime) {
            Mem:removeVector(Contact)
            SeenTime:removeNumber(Contact)
        }
    }
    
    #####
    
    for (I=1, Mult) {
        if (World==1) {
            rangerIgnoreWorld(0)}
        else {
            rangerIgnoreWorld(1)}
        rangerHitWater(Water)
            
        rangerDefaultZero(1) 
        XO = (X * Inc - 1) / Zoom
        YO = (Y * Inc - 1) / Zoom
        R = ranger(Range, XO, YO)
        Eid=R:entity():id()
        Pos=R:position()
        D = R:distance()
        #if (D < FindMin) {FindMin = D}
        if (D > FindMax) {FindMax = D}
        if (X < 32 & Y < 32) {
            G=(D/FindMax) * 255
            if (!R:entity():isNPC()) {LCD:writeCell(X + Y * 32, G)}  #if Eid==0
            else {
                LCD:writeCell(X + Y * 32, 50000)
                Mem[toString(Eid),vector]=Pos
                SeenTime[toString(Eid),number]=curtime()
            }
        }
        LID=Eid
        X++
        if (X >= Res) {X = -1, Y++}
        if (Y >= Res) {
            Y = -1
            MinDepth = FindMin
            MaxDepth = FindMax
            FindMin = D
            FindMax = D
        }
    }

}
