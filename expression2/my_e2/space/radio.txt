@name radio
@inputs On TxOn RadioIn:wirelink RadioOut:wirelink In1 In2 In3
@outputs Tx TimeLeft DBm1 InData TimeLastIn Code
@outputs Out1 Out2 Out3 
@persist Count Count2 Tx Mem:array DBm1
@trigger None
interval(250)
Time=0.5
Delay=5
Code=133713371337

if (On==1) {Count2=Count2+0.25
    if (TxOn==1) {Count=Count+0.25
    if (Count>=Delay) {Tx=1}
    if (Count>=Delay+Time) {Count=0
        Tx=0}
    }
    
}
else {Tx=0
    Count=0}
TimeLastIn=Count2
TimeLeft=Delay-Count

Ch1=RadioIn["Channel1",number]
if (Ch1==Code) {Count2=0
    DBm1=(RadioIn["Ch1_dBm",number]+RadioIn["Ch2_dBm",number]+RadioIn["Ch3_dBm",number]+RadioIn["Ch4_dBm",number])/4
    Mem[1,number]=RadioIn["Channel2",number]
    Mem[2,number]=RadioIn["Channel3",number]
    Mem[3,number]=RadioIn["Channel4",number]
}
Out1=Mem[1,number]
Out2=Mem[2,number]
Out3=Mem[3,number]

RadioOut["Channel1",number] = Code
RadioOut["Channel2",number] = In1
RadioOut["Channel3",number] = In2
RadioOut["Channel4",number] = In3

