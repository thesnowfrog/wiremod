@name Joystick controller
@inputs X Y
@outputs Xc Yc
@persist 
@trigger X Y

Xc = (X-0.5) * 2
Yc = (Y-0.5) * 2
