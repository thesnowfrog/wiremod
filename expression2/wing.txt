@name wing
@inputs Wing:entity Wing2:entity Wing3:entity Wing4:entity
@outputs Area Area2 V VV:vector LV:vector FrontVel Front:vector AoA Sideslip LiftForce WingForce Stall
@persist 
@trigger 
# Wing2 optional
interval(10)

WingMul = 8
PropRotation = 0 # yaw rotation of prop

Area = sqrt(Wing:boxSize():x()*Wing:boxSize():y())
Area2 = sqrt(Wing3:boxSize():x()*Wing3:boxSize():y())
LV = Wing:velL()

VV = Wing:pos()+(Wing:vel())
FrontVel = abs(vec2(LV):rotate(PropRotation):x())
Sideslip = Wing:bearing(VV)
AoA = Wing:elevation(VV)

if (AoA>=40 | AoA<= -40 | Sideslip>=40 | Sideslip<= -40) {
    LiftForce=0
    Stall = 1}
else {LiftForce = WingMul*sin(AoA*6)*cos(Sideslip)
    Stall = 0}

WingForce = (FrontVel * WingMul)*LiftForce
WingForce2 = (FrontVel * WingMul)*(LiftForce/2)

Wing:applyForce( Wing:up() * -WingForce )
Wing2:applyForce( Wing2:up() * WingForce )
Wing3:applyForce( Wing3:up() * WingForce2 )
Wing4:applyForce( Wing4:up() * WingForce2 )
