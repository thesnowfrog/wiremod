@name UST Weapons Control
@inputs Arm Fire Ready1 Ready2 Target:entity TargetPos:vector Shots1 Shots2
@outputs Tgt:entity TgtPos:vector Hit Ready Reload Launch1 Launch2 Timer Launch Cloaking MoveAllow
@persist Timer Tgt:entity TgtPos:vector Launch Launched1 Launched2 Reload
@trigger None
interval(100)

Hit = Target!= noentity()
Ready = (Timer==0 & !Launch & (Ready1 & Ready2))
Cloaking = Arm & !Launch
MoveAllow = Arm & !Launch
if (!Shots1 | !Shots2) {Reload=1}
if (Shots1==4 & Shots2==4) {Reload=0}

if (Arm) {
    if (Hit & !Launch) {
        TgtPos = TargetPos + vec(0,0,(Target:height()*0.5))
        Tgt = Target
    }
        
    if (Fire & Ready & !Reload) {Launch=1}
    
    if (Launch) {
        if (Launch & Ready1 & !Launched1 & Timer>=0) {
            Launch1 = 1
            Launched1 = 1
        }
        else {Launch1 = 0}
        
        if (Launch & Ready2 & !Launched2 & Timer>=0.5) {
            Launch2 = 1
            Launched2 = 1
        }
        else {Launch2 = 0}
        
        if (Launch & Timer>=5) {
            Launch=0
            Launch1 = 0
            Launch2 = 0
            Launched1 = 0
            Launched2 = 0
            Timer = 0}
        else {Timer += 0.1}
    }
}
else {
    Launch1 = 0
    Launch2 = 0
    Launch = 0
    Launched1 = 0
    Launched2 = 0
    Timer = 0
}
