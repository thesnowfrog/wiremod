@name XSB Weapons Bus
@inputs Arm Fire Reset Ready1 Ready2 TargetPos:vector Salvo MaxEngagements
@outputs TgtPos:vector Ready Launch1 Launch2 Timer Launch Launched1 Launched2 Expended SalvoMode PylonCounter Engagements 
@outputs AttackFinished Fired
@persist Timer Launch Launched1 Launched2 Expended SalvoMode PylonCounter Fired Engagements
@trigger Fire Reset
interval(100)

# Pylons:
# 3 1  2 4

TgtPos = TargetPos
Ready = (Timer==0 & !Launch & ((Ready1 & !Launched1) | (Ready2 & !Launched2)))

if (Arm) {
    if (AttackFinished) {AttackFinished = 0}
    if (Fire & Salvo==2) {Launch=1}
    if (Fire & Salvo==1) {SalvoMode=1}
    
    if (SalvoMode & Engagements<MaxEngagements) {
        AttackFinished = 0
        
        if (PylonCounter==0 & Ready1) {
            Launch1 = 1
            Launched1 = 1
            Fired += 1
        }
        elseif (PylonCounter==1 & Ready2) {
            Launch2 = 1
            Launched2 = 1
            Fired += 1
        }
        
        if (Fired==Salvo) {
            PylonCounter=0
            SalvoMode=0
            Fired=0
            Engagements+=1
            AttackFinished = 1
        }
        PylonCounter+=1
    }
    
    if (Launch) {
        AttackFinished = 0
        if (Launch & Timer>=0) {
            Launch1 = 1
            Launched1 = 1
        }
        if (Launch & Timer>=0.5) {
            Launch2 = 1
            Launched2 = 1
        }
        if (Launch & Timer>=1) {
            Expended = 1
        }
        Timer += 0.2
        if (Launch & Timer>=5) {Launch=0}
        AttackFinished = 1
    }
    else {
        Expended = (Launched1 & Launched2) & (!Ready1 & !Ready2)
        Timer = 0
        }
}
else {
    Expended = (Launched1 & Launched2) & (!Ready1 & !Ready2)
    Launch1 = 0
    Launch2 = 0
    Launch = 0
}
if (Reset) {
    Launch = 0
    Launch1 = 0
    Launch2 = 0
    Launched1 = 0
    Launched2 = 0
    Timer = 0
    Expended = 0
    Engagements = 0
    PylonCounter=0
    SalvoMode=0
    AttackFinished = 0
}
