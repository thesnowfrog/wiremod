@name Mortar Ballistic
@inputs Angle TP:vector
@outputs Dist Bear 
@persist 
@trigger 

interval(100)
E=entity()
Dist = E:pos():distance(TP)
Bear = E:bearing(TP)
