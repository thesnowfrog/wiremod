@name wuflu
@inputs
@outputs Infected
@persist InfectedT:table Started
@trigger 

interval(1000)
Stage1 = 30 #asymptomatic 30
Stage2 = Stage1+60 #symptomatic 90
Stage3 = Stage2+60 #disease 150
Stage4 = Stage3+60 #terminal 210

Len = 10000
SpreadRange = 100
Infected = InfectedT:count()

function void spreadVirus(Ent:entity, InfectChance) {
    findClearBlackList() 
    findIncludeClass("player")
    findIncludeClass("npc_citizen")
    findIncludeClass("npc_combine_s")
    findIncludeClass("npc_metropolice")
    findExcludeEntity(Ent)
    findInSphere(Ent:pos(), SpreadRange)
    findSortByDistance(Ent:pos())
    F2 = findToArray()
    foreach(K,TGT:entity = F2) {
        IID = TGT:id()
        Dist = Ent:pos():distance(TGT:pos())
        if (Dist<(SpreadRange/2)) {InfectChanceX=InfectChance*2}
        else {InfectChanceX=InfectChance}
        Already = InfectedT:exists(IID:toString())
        
        if (randint(0,99)<=InfectChanceX & !Already) {
            InfectedT[IID:toString(),number]=time()
        }
    }
}

if (Started & Infected==0) {selfDestruct()}

if (!Started) {
    Mypos=entity():pos()
    findIncludeClass("player")
    findIncludeClass("npc_citizen")
    findIncludeClass("npc_combine_s")
    findIncludeClass("npc_metropolice")
    findInSphere(Mypos, Len)
    findSortByDistance(Mypos)
    F = findToArray()
    
    if (F:count()!=0) {
        IID = F[1,entity]:id()
        InfectedT[IID:toString(),number]=time()
        Started = 1
        entity():soundPlay(1,5,"hl1/fvox/biohazard_detected.wav")
    }
}


foreach(K,T:number = InfectedT) {
    EID = K:toNumber()
    E = entity(EID)
    
    if (E:health()<=0) {
        InfectedT:remove(K)
        }
    else {
        TI = time()-InfectedT[K,number]
        
        if (TI>=Stage1 & TI<Stage2) {
            E:setColor(255,255,254)
            InfectChance=4
            Stage=1
            }
        elseif (TI>=Stage2 & TI<Stage3) {
            E:setColor(255,255,253)
            Coofchance=9
            InfectChance=9
            Stage=2
            }
        elseif (TI>=Stage3 & TI<Stage4) {
            E:setColor(255,255,252)
            Coofchance=19
            InfectChance=14
            Stage=3
            }
        elseif (TI>=Stage4) {
            E:setColor(255,255,250)
            Coofchance=24
            InfectChance=19
            Stage=4
            }
        else {Stage=0}
        
        if (Stage>=1) {
            spreadVirus(E,InfectChance)
        }
        
        if (Stage>=2) {
            
            Coof=randint(0,99)
            if (Coof<=Coofchance) {
                
                if (Stage>=3) {Coofrand = randint(0,5)}
                else {Coofrand = randint(0,3)}
                
                if (Coofrand==0) {Coofsound = "ambient/voices/cough1.wav"}
                elseif (Coofrand==1) {Coofsound = "ambient/voices/cough2.wav"}
                elseif (Coofrand==2) {Coofsound = "ambient/voices/cough3.wav"}
                elseif (Coofrand==3) {Coofsound = "ambient/voices/cough4.wav"}
                elseif (Coofrand==4) {Coofsound = "ambient/voices/citizen_beaten3.wav"}
                elseif (Coofrand==5) {Coofsound = "ambient/voices/citizen_beaten4.wav"}
                E:soundPlay(E:id(),3,Coofsound)
                spreadVirus(E,InfectChance*2)
                }
                
            if (Stage==3) {if (randint(0,9)==0) {E:takeDamage(1)}}
            elseif (Stage==4) {
                if (randint(0,9)<=1) {E:takeDamage(5)}
                Coof=randint(0,99)
                if (Coof<=4) {
                    Coofrand = randint(0,4)
                    if (Coofrand==0) {Sicksound = "vo/npc/male01/moan01.wav"}
                    elseif (Coofrand==1) {Sicksound = "vo/npc/male01/moan02.wav"}
                    elseif (Coofrand==2) {Sicksound = "vo/npc/male01/moan03.wav"}
                    elseif (Coofrand==3) {Sicksound = "vo/npc/male01/moan04.wav"}
                    elseif (Coofrand==4) {Sicksound = "vo/npc/male01/moan05.wav"}
                    E:soundPlay(E:id()+1000,3,Sicksound)
                }
            }
        }
    }
}
