@name E2 Steerable Directional Target Finder
@inputs On Index Reset TPos:vector ConeAngle
@inputs NPCs Players Props Locators Vehicles ExcludePlayer ExcludePlayerProps
@outputs Bogeys Entity:entity ID Distance Type:string Model:string Position:vector Velocity:vector 
@outputs IsPlayer IsNPC IsVehicle F:array Dir:vector
@persist 
@trigger None

#Exclude/Allow = blacklist
#Include/Disallow = whitelist
#blacklist priority
if (On) {interval(100)}
else {interval(500)}

Ind=Index+1
if (Reset) {reset()}
if (first()|duped()) {
    findExcludeEntity(entity())
    findExcludeClass("beam")
    findExcludeClass("point_spotlight")
    findExcludeClass("spotlight_end")
    findExcludeClass("func_button")
    findExcludeClass("func_door")
    findExcludeClass("path_track")
    findExcludeClass("lua_run")
    findExcludeClass("path_corner")
    findExcludeClass("func_brush")
    findExcludeClass("npc_heli_avoidbox")
    findExcludeClass("ambient_generic")
    findExcludeClass("trigger_multiple") 
    findExcludeClass("vehicle") ######
    findExcludeClass("gmod_camera")
    findExcludeClass("hologram")
    findExcludeClass("gmod_tool")
    findExcludeClass("pist_weapon")
    findExcludeClass("ammo")
    findExcludeClass("weapon")
    findExcludeModel("weapon")
    findExcludeClass("gb5")
    findExcludeClass("env")

}
if (On) {
    E=entity()
    Owner=owner()
    Mypos=E:pos()
    Len=100000
    if (ConeAngle==0) {Deg=30}
    else {Deg=ConeAngle}
    MyAng=E:angles()
    
    if (ExcludePlayer) {findExcludePlayer(Owner)}
    else {findIncludePlayer(Owner)}
    
    if (ExcludePlayerProps) {findExcludePlayerProps(Owner)}
    else {findIncludePlayerProps(Owner)}
    
    if (Players) {findAllowClass("player")}
    else {findExcludeClass("player")}

    if (Vehicles) {findAllowClass("vehicle")}
    else {findExcludeClass("vehicle")}
    
    if (NPCs) {findAllowClass("npc")}
    else {findExcludeClass("npc")}
    
    if (Props) {
        findAllowClass("prop")
        findAllowClass("acf")
        findAllowClass("wiremod")
        findAllowClass("gmod_wire")
    }
    else {
        findExcludeClass("prop")
        findExcludeClass("acf")
        findExcludeClass("wiremod")
        findExcludeClass("gmod_wire")
    }
    
    if (Locators) {findAllowClass("gmod_wire_locator")}
    else {findExcludeClass("gmod_wire_locator")}
    
    if (TPos==vec()) {Dir = E:up()
        #SafeaimvecX=entity():pos():x()+(cos(entity():angles():yaw()+180))*10000
        #SafeaimvecY=entity():pos():y()+(sin(entity():angles():yaw()+180))*10000
        #SafeaimvecZ=entity():pos():z()+(cos(entity():angles():pitch()-135))*10000
        #Dir = vec(SafeaimvecX,SafeaimvecY,SafeaimvecZ)
    }
    else {Dir = (TPos-E:pos()):normalized()}
    findInCone(Mypos, Dir, Len, Deg)
    findSortByDistance(Mypos)
    F = findToArray()
    Bogeys = F:count()
    Entity = F[Ind,entity]
    IsPlayer = Entity:isPlayer()
    IsNPC = Entity:isNPC()
    IsVehicle = Entity:isVehicle()
    ID = Entity:id()
    Type = Entity:type()
    Model = Entity:model()
    Position = Entity:pos()
    Velocity = Entity:vel()
    Distance = Mypos:distance(Position)
}
else{
    Entity = entity(0)
    IsPlayer = 0
    IsNPC = 0
    IsVehicle = 0
    ID = 0
    Type = ""
    Model = ""
    Position = vec()
    Velocity = vec()
    Distance = inf()
}
